var Configurations  = require('../configurations/Configurations'),
    Assets          = require('../managers/Assets');

var $game,
    $configurations, $assets;


class BoardController {

    constructor(game){
        $game = game;

        $configurations = new Configurations($game);
        $assets         = new Assets($game);
    }

    make(){

    }

    check(){

    }
}



module.exports = BoardController;
